//
//  SearchViewController.swift
//  IMDB Client
//
//  Created by Ivan on 15/10/2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class SearchViewController: UITableViewController {
    
    
    @IBOutlet weak var searchTextField: UITextField!
    var searchResult:SearchListResponse?
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = 100
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return searchResult?.Search.count ?? 0
    }
//Вывод ячейки таблицы
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MovieViewCell
        cell.titleLabel.text = searchResult!.Search[indexPath.row].title
        cell.yearLabel.text = searchResult!.Search[indexPath.row].year
        if searchResult?.Search[indexPath.row].poster != nil{
            let url = URL(string: (searchResult?.Search[indexPath.row].poster)!)
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    cell.imageCell.image = UIImage(data: data!)
                }
            }
        }
        return cell
    }
//функция поиска
    func search(name: String){
        let urlString = "http://www.omdbapi.com/?s=\(name)&apikey=\(apiKey)"
        print (urlString)
        guard let url = URL(string: urlString) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {return}
            do {
                let sourseList = try JSONDecoder().decode(SearchListResponse
                    .self, from: data)
                DispatchQueue.main.async {
                    self.searchResult = sourseList
                    self.tableView?.reloadData() // обновление таблицы
                }
            } catch let error {
                print("Не парсит JSON \(error)")
            }
        }.resume()
    }

    @IBAction func searchButton(_ sender: Any) {
        search(name: searchTextField.text ?? "spider")
        view.endEditing(true)
    }
//Передача значений в segue
    override func prepare (for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "deteilSegue"{
            if let indexPath = tableView.indexPathForSelectedRow{
                let dvc = segue.destination as! DetailViewController
                if (searchResult?.Search[indexPath.item].title) != nil{
                    dvc.id = searchResult!.Search[indexPath.item].imdbID
                }
            }
        }
    }
}
