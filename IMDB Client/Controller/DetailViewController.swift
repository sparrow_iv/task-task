//
//  DetailViewController.swift
//  IMDB Client
//
//  Created by Ivan on 16/10/2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var idMovie: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var movieRating: UILabel!
    @IBOutlet weak var movieDirector: UILabel!
    @IBOutlet weak var movieCountry: UILabel!
    @IBOutlet weak var movieActors: UILabel!
    @IBOutlet weak var moviePlot: UILabel!
    
    var id = ""
    var info : movie?
    override func viewDidLoad() {
        super.viewDidLoad()
        idMovie.text = id
        let urlString = "http://www.omdbapi.com/?i=\(id)&apikey=\(apiKey)"
        
        print (urlString)
        guard let url = URL(string: urlString) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {return}
            do {
                let sourseList = try JSONDecoder().decode(movie.self, from: data)
                DispatchQueue.main.async {
                    self.info = sourseList 
                    // print(sourseList) //проверка распарсеного результата
                    self.movieTitle.text = self.info?.title
                    self.movieRating.text = self.info?.imdbRating
                    self.movieDirector.text = self.info?.director
                    self.movieCountry.text = self.info?.country
                    self.movieActors.text = self.info?.actors
                    self.moviePlot.text = self.info?.plot
                    let url = URL(string: (self.info?.poster)!)
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            self.moviePoster.image = UIImage(data: data!)
                        }
                    }
                }
            } catch let error {
                print("Не парсит JSON \(error)")
            }
        }.resume()
    }
}
