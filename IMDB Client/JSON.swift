//
//  File.swift
//  IMDB Client
//
//  Created by Ivan on 15/10/2018.
//  Copyright © 2018 Ivan. All rights reserved.
//
import UIKit
import Foundation

// Тут структeра данных
let apiKey = "f4db61c1"

struct movie:Decodable {
    var actors : String
    var awards : String
    var boxOffice : String
    var country : String
    var director : String
    var DVD : String
    var genre : String
    var imdbID : String
    var imdbRating : String
    var imdbVotes : String
    var language : String
    var metascore : String
    var plot : String
    var poster : String
    var production : String
    var rated : String
    var realeased : String
    var response : String
    var runtime : String
    var title : String
    var website : String
    var writer : String
    var year : String
   

    
    enum CodingKeys: String, CodingKey{
        case Actors
        case Awards
        case BoxOffice
        case Country
        case Director
        case DVD
        case Genre
        case imdbID
        case imdbRating
        case imdbVotes
        case Language
        case Metascore
        case Plot
        case Poster
        case Production
        case Rated
        case Released
        case Response
        case Runtime
        case Title
        case Website
        case Writer
        case Year
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.actors = try container.decode(String.self, forKey: .Actors)
        self.awards = try container.decode(String.self, forKey: .Awards)
        self.boxOffice = try container.decode(String.self, forKey: .BoxOffice)
        self.country = try container.decode(String.self, forKey: .Country)
        self.director = try container.decode(String.self, forKey: .Director)
        self.DVD = try container.decode(String.self, forKey: .DVD)
        self.genre = try container.decode(String.self, forKey: .Genre)
        self.imdbID = try container.decode(String.self, forKey: .imdbID)
        self.imdbRating = try container.decode(String.self, forKey: .imdbRating)
        self.imdbVotes = try container.decode(String.self, forKey: .imdbVotes)
        self.language = try container.decode(String.self, forKey: .Language)
        self.metascore = try container.decode(String.self, forKey: .Metascore)
        self.plot = try container.decode(String.self, forKey: .Plot)
        self.poster = try container.decode(String.self, forKey: .Poster)
        self.production = try container.decode(String.self, forKey: .Actors)
        self.rated = try container.decode(String.self, forKey: .Rated)
        self.realeased = try container.decode(String.self, forKey: .Released)
        self.response = try container.decode(String.self, forKey: .Response)
        self.runtime = try container.decode(String.self, forKey: .Runtime)
        self.title = try container.decode(String.self, forKey: .Title)
        self.website = try container.decode(String.self, forKey: .Website)
        self.writer = try container.decode(String.self, forKey: .Writer)
        self.year = try container.decode(String.self, forKey: .Year)
        
    }
}

struct  SearchListResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case Search
    }
    let Search: [movieResponse]
}
struct movieResponse: Decodable {
    var imdbID: String
    var poster: String
    var title: String
    var year: String
    
    
    enum CodingKeys: String, CodingKey {
        case imdbID
        case Poster
        case Title
        case Year
    }
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.imdbID = try container.decode(String.self, forKey: .imdbID)
        self.poster = try container.decode(String.self, forKey: .Poster)
        self.title = try container.decode(String.self, forKey: .Title)
        self.year = try container.decode(String.self, forKey: .Year)
    }
}
